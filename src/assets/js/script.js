//JQuery (look at webpack.config.js)
const $ = require('jquery');
//bootstrap
import 'bootstrap';
//sass
import '../css/style.scss';
//css
import '../css/style.css';
//slick library (carousel)
import 'slick-carousel';

/* Sommary :
1- Function DETAILS
2- Function View more genre
3- Popular Movies
4- Genres
5- Search
*/

let $accueil = $('#accueil');
let $movie_details = $('#movie_details');
let $search_movies_div = $('#search_movies_div');
let $movies_list = $('.movies_list');

/*******************************************
 *                                         *
 * -----------FUNCTION-DETAILS-------------*
 *                                         *
 *******************************************/

//Create a function for show movie's details
function showDetails(id, release_date, title, movies_div, where_back) {
    //Target the one_movie class
    let $one_movie = $('.popular_image', movies_div);
    $one_movie.on('click', function () {
        //console.log($id); // The id is good for each film
        $.get(`https://api.themoviedb.org/3/movie/${id}?api_key=d6796e2b0081b18c9657c0ab61f78136&language=en-US
        `, function (response) {
            //console.log(response); // response is an array with many details for each film

            //Create variable with informations
            let $movie_overview = response.overview;
            let $movie_vote_average = response.vote_average * 10;
            let $movie_poster_path = response.poster_path;
            let $poster_path;

            //Create condition if a result give a movie without backdrop_path
            if ($movie_poster_path == null) {
                $poster_path = '<img class="poster_image" src="../src/assets/img/backdrop_not_found.jpg"/>';
            }
            else {
                $poster_path = `<img class="poster_image" src="https://image.tmdb.org/t/p/w500${$movie_poster_path}"/>`;
            }

            //Create a template for the details

            let movie_tpl = `<div class="title_button"><h2>${title}</h2><button class="back_button">Back</button></div>
            <span>Release Date : ${release_date}</span>
            <div class="info_movie">
                <div class="overview"><h3>Plot: </h3>
                ${$movie_overview}</div>
                <div class="poster">${$poster_path}</poster>
            </div>
            <div class="percent">
                <svg>
                    <circle cx="70" cy="70" r="70"></circle>
                    <circle cx="70" cy="70" r="70"></circle>
                </svg>
                <div class="number">
                    <span>${$movie_vote_average}%</span>
                </div>
            </div>
            `;

            //Hide the accueil section with a class
            $accueil.addClass('d-none');
            $search_movies_div.addClass('d-none');
            //Remove the d-none classe for the div with details
            $movie_details.removeClass('d-none');

            //Add the template in a div
            $movie_details.html(movie_tpl);

            let rateCircle = $('.percent');

            //Use different color for rate
            if ($movie_vote_average > 65) {
                rateCircle.css('background', '#0b971e');
            } else if ($movie_vote_average <= 65 && $movie_vote_average > 45) {
                rateCircle.css('background', '#d8a006');
            } else {
                rateCircle.css('background', '#ca3126');
            }

            //Add an event for the back button. target it 
            let $details_back_button = $('.back_button');

            //Add an event listener
            $details_back_button.on('click', function () {
                //The event hide the details div, and remove the d-none class for the accueil
                where_back.removeClass('d-none');
                $movie_details.addClass('d-none');
            });
        });
    });

}

/*******************************************
 *                                         *
 * -------FUNCTION-VIEW-MORE-GENRES--------*
 *                                         *
 *******************************************/

function viewMoreGenres($genre_id) {

    $.get(`https://api.themoviedb.org/3/discover/movie?api_key=d6796e2b0081b18c9657c0ab61f78136&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_genres=${$genre_id}`, function (response) {
        //Create a while for show movies
        for (const movie of response.results) {
            //Get details for each movie
            let $title = movie.title;
            let $backdrop_path = movie.backdrop_path;
            let $release_date = movie.release_date;
            let $id = movie.id;
            let $backdrop;

            //Create condition if a result give a movie without backdrop_path
            if ($backdrop_path == null) {
                $backdrop = '<img class="popular_image" src="../src/assets/img/backdrop_not_found.jpg"/>';
            }
            else {
                $backdrop = `<img class="popular_image" src="https://image.tmdb.org/t/p/w500${$backdrop_path}"/>`;
            }

            //Create a template for each movies
            let tpl = `
        <h3 class="popular_title">${$title}</h3>
        <span class="popular_release_date">${$release_date}</span>
        ${$backdrop}`;

            //Create a div including the template
            let search_div = $('<div />', {
                html: tpl
            });

            //Add a class for each div
            search_div.attr('class', 'one_movie');
            //Add each div into the popular section
            $movies_list.append(search_div);

            //Show details
            showDetails($id, $release_date, $title, search_div, $search_movies_div);

        }
    });

}


/*******************************************
 *                                         *
 * ------------POPULAR-MOVIES--------------*
 *                                         *
 *******************************************/

//Target DOM elements
let $popular_movies = $('.carousel_popular_movies');

//Call API to Movies Database
$.get('https://api.themoviedb.org/3/movie/popular?api_key=d6796e2b0081b18c9657c0ab61f78136&language=en-US&page=1', function (response) {
    //console.log(response); //The console log give us an array with 20 most popular movies

    //Make a while to get many elements for each movie
    for (const movie of response.results) {
        //Create variables with elements of movies
        let $title = movie.title;
        let $backdrop_path = movie.backdrop_path;
        let $release_date = movie.release_date;
        let $id = movie.id;
        let $backdrop;

        //Create condition if a result give a movie without backdrop_path
        if ($backdrop_path == null) {
            $backdrop = '<img class="popular_image" src="../src/assets/img/backdrop_not_found.jpg"/>';
        }
        else {
            $backdrop = `<img class="popular_image" src="https://image.tmdb.org/t/p/w500${$backdrop_path}"/>`;
        }

        //Create a template for each movies
        let tpl = `
            <h3 class="popular_title">${$title}</h3>
            <span class="popular_release_date">${$release_date}</span>
            ${$backdrop}`;


        //Create a div including template for each movies
        let popular_movies_div = $('<div />', {
            html: tpl
        });
        //Add a class for each div
        popular_movies_div.attr('class', 'one_movie');
        //Add each div into the popular section
        $popular_movies.append(popular_movies_div);

        //Now, get informations for each movie when we click on the pic of the movie
        showDetails($id, $release_date, $title, popular_movies_div, $accueil);//Go on the top to read/edit the function

    }
    //make a carousel with slick library
    $popular_movies.slick({
        centerPadding: '60px',
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows: false,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerPadding: '40px',
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerPadding: '40px',
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }
        ]
    });
});

/*******************************************
 *                                         *
 * ----------------GENRES------------------*
 *                                         *
 *******************************************/

//Target DOM element 
let $genres_movies = $('#genres');
let $search_by_genre = $('#search_by_genre');

//Call an API to get all genres of movies
$.get('https://api.themoviedb.org/3/genre/movie/list?api_key=d6796e2b0081b18c9657c0ab61f78136&language=en-US', function (response) {
    //console.log(response); //The console log give us an array with 19 genres

    //Make a while to make one category per genre
    for (const genre of response.genres) {
        //Get names and id
        let $genre_name = genre.name;
        let $genre_id = genre.id;

        //Create a template
        let tpl = `
        <div class="genre_name">
            <h2>${$genre_name}</h2> 
            <button class="view_more_button ${$genre_id}"">View More</button>
        </div>
        <div id="${$genre_id}_movies" class="carousel carousel_genre_movies">
        
        </div>
        `;

        let tpl2 = `<span>${$genre_name}</span>`;

        //Create a div including the template for each genres
        let category_div = $('<div />', {
            html: tpl
        });

        //Create li including the second template
        let category_nav = $('<li />', {
            html: tpl2
        });
        //Add a class for each div
        category_div.attr('class', 'category_div');
        //Add a class for each li
        category_nav.attr('class', `category_nav ${$genre_id}`);

        //Add each div into the section for genres
        $genres_movies.append(category_div);

        //Add each li into the ul
        $search_by_genre.append(category_nav);

        //Target the new div 
        let $genre_id_movies = $(`#${$genre_id}_movies`);

        //Add an eventlistener on the view more button
        $(`.${$genre_id}`).on('click', function () {
            //Hide accueil and details, and show the div search
            $accueil.addClass('d-none');
            $movie_details.addClass('d-none');
            $movies_list.html('');
            $search_movies_div.removeClass('d-none');
            $movies_list.removeClass('d-none');
            $search_movies_div.addClass('flex_column');
            $('.no_result').html('');
            viewMoreGenres($genre_id);
        });

        //Call an API for get movies for each genres
        $.get(`https://api.themoviedb.org/3/discover/movie?api_key=d6796e2b0081b18c9657c0ab61f78136&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_genres=${$genre_id}`, function (response) {
            //Make a while for each movie fund
            for (let i = 0; i < 6; i++) {
                //Get details for each movie
                let $title = response.results[i].title;
                let $backdrop_path = response.results[i].backdrop_path;
                let $release_date = response.results[i].release_date;
                let $id = response.results[i].id;
                let $backdrop;

                //Create condition if a result give a movie without backdrop_path
                if ($backdrop_path == null) {
                    $backdrop = '<img class="popular_image" src="../src/assets/img/backdrop_not_found.jpg"/>';
                }
                else {
                    $backdrop = `<img class="popular_image" src="https://image.tmdb.org/t/p/w500${$backdrop_path}"/>`;
                }

                //Create a template for each movies
                let tpl = `
                    <h3 class="popular_title">${$title}</h3>
                    <span class="popular_release_date">${$release_date}</span>
                    ${$backdrop}`;

                //Create a div including the template
                let genre_div = $('<div />', {
                    html: tpl
                });

                //Add a class for each div
                genre_div.attr('class', 'one_movie');
                //Add each div into the popular section
                $genre_id_movies.append(genre_div);

                //Show details
                showDetails($id, $release_date, $title, genre_div, $accueil);

            }
            //make a carousel with slick library
            $genre_id_movies.slick({
                centerPadding: '60px',
                slidesToShow: 3,
                slidesToScroll: 3,
                arrows: false,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            arrows: false,
                            centerPadding: '40px',
                            slidesToShow: 2,
                            slidesToScroll: 2,
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            arrows: false,
                            centerPadding: '40px',
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    }
                ]
            });
        });

    }
});


/*******************************************
 *                                         *
 * ----------------SEARCH------------------*
 *                                         *
 *******************************************/

//Target form and it elements
let $search_form = $('.search_form');
let $search_text = $('.search_text');

//Create an event listener for the submit of this form
if ($search_text.val() !== null) {

    $search_form.on('submit', function (e) {
        e.preventDefault();
        //Create a variable for the value in the text input
        let search_value = $search_text.val();
        //console.log(search_value); //The console show us the good value

        //Hide accueil and details, and show the div search
        $accueil.addClass('d-none');
        $movie_details.addClass('d-none');
        $movies_list.html('');
        $search_movies_div.removeClass('d-none');
        $movies_list.removeClass('d-none');
        $search_movies_div.addClass('flex_column');
        $('.no_result').html('');

        //make a request to movie databe for the research of movies
        $.get(`https://api.themoviedb.org/3/search/movie?api_key=d6796e2b0081b18c9657c0ab61f78136&language=en-US&query=${search_value}&page=1&include_adult=false
    `, function (response) {
            //console.log(response); //The response is an array with movies, and a number of results and pages    
            //Create a while for show movies
            for (const movie of response.results) {
                //Get details for each movie
                let $title = movie.title;
                let $backdrop_path = movie.backdrop_path;
                let $release_date = movie.release_date;
                let $id = movie.id;
                let $backdrop;

                //Create condition if a result give a movie without backdrop_path
                if ($backdrop_path == null) {
                    $backdrop = '<img class="popular_image" src="../src/assets/img/backdrop_not_found.jpg"/>';
                }
                else {
                    $backdrop = `<img class="popular_image" src="https://image.tmdb.org/t/p/w500${$backdrop_path}"/>`;
                }

                //Create a template for each movies
                let tpl = `
                    <h3 class="popular_title">${$title}</h3>
                    <span class="popular_release_date">${$release_date}</span>
                    ${$backdrop}`;

                //Create a div including the template
                let search_div = $('<div />', {
                    html: tpl
                });

                //Add a class for each div
                search_div.attr('class', 'one_movie');
                //Add each div into the popular section
                $movies_list.append(search_div);

                //Show details
                showDetails($id, $release_date, $title, search_div, $search_movies_div);

            }
        });
    });
}